import React from "react";


const RenderValues = ({values}) => {
    const listValues = values.map((number) =>
        <li key={number.toString()}>
            {number}
        </li>
    );
    return <ul>{listValues}</ul>;
};

export default RenderValues
