import React, {Component} from "react";
import defaultValues from '../ConstsContainer';


class SearchValue extends Component {
    constructor(props) {
        super(props);

        this.state = {
            tempText: "",
            searchedArray: [],
        }
    }

     handleChange(e){
         this.setState({tempText: e.currentTarget.value,
             searchedArray: defaultValues.reduce((newArray, tempValue) => e.currentTarget.value.length &&
             tempValue.indexOf(e.currentTarget.value) !== -1 ?  [...newArray, tempValue] : newArray, [])})
    };

    searchSubString(value){
        alert(defaultValues.find(tempElement => value === tempElement) ?
            defaultValues.find(tempElement => value === tempElement):"Not Found");
    };

    render() {
        return <div>
            <button className="search-value" onClick={() => this.searchSubString(this.state.tempText)}>Search</button>
            <input type="text" onChange={this.handleChange.bind(this)} value={this.state.tempText}></input>
            {this.state.searchedArray.join(" ")}
        </div>
    }
}


export default SearchValue
