import React, {Component} from 'react';


import RenderValues from './components/DataMapping.js'
import SearchValue from './components/SearchValue.js'
import defaultValues from './ConstsContainer.js'


class App extends Component{
    render() {
    return <div>
        <RenderValues values={defaultValues} />
        <SearchValue/>
        </div>
    }
}

export default App
